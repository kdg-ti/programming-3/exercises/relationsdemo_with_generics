package be.kdg.java3.relationsdemo.domain;

import java.util.ArrayList;
import java.util.List;

public class Course implements Entity{
    private int id;
    private String name;
    private int academicYear;

    private List<Student> students = new ArrayList<>();

    public Course(int id, String name, int academicYear) {
        this.id = id;
        this.name = name;
        this.academicYear = academicYear;
    }

    public Course(String name, int academicYear) {
        this.name = name;
        this.academicYear = academicYear;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAcademicYear() {
        return academicYear;
    }

    public void setAcademicYear(int academicYear) {
        this.academicYear = academicYear;
    }

    public List<Student> getStudents() {
        return students;
    }

    public void setStudents(List<Student> students) {
        this.students = students;
    }

    public void addStudent(Student student) {
        students.add(student);
    }

    @Override
    public String toString() {
        return "Course{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", academicYear=" + academicYear +
                '}';
    }
}
