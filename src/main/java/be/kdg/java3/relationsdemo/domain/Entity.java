package be.kdg.java3.relationsdemo.domain;

public interface Entity {
    int getId();
    void setId(int id);
}
