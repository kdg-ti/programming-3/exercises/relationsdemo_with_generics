package be.kdg.java3.relationsdemo.domain;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

public class Student implements Entity{
    private int id;
    private String name;
    private double length;
    private LocalDate birthday;

    private Address address;
    private School school;
    private List<Course> courses = new ArrayList<>();

    public Student(int id, String name, double length, LocalDate birthday) {
        this.id = id;
        this.name = name;
        this.length = length;
        this.birthday = birthday;
    }

    public Student(String name, double length, LocalDate birthday) {
        this.name = name;
        this.length = length;
        this.birthday = birthday;
    }

    public School getSchool() {
        return school;
    }

    public void setSchool(School school) {
        this.school = school;
        this.school.addStudent(this);
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getLength() {
        return length;
    }

    public void setLength(double length) {
        this.length = length;
    }

    public LocalDate getBirthday() {
        return birthday;
    }

    public void setBirthday(LocalDate birthday) {
        this.birthday = birthday;
    }

    public Address getAddress() {
        return address;
    }

    public void setAddress(Address address) {
        this.address = address;
        this.address.setStudent(this);
    }

    public List<Course> getCourses() {
        return courses;
    }

    public void setCourses(List<Course> courses) {
        this.courses = courses;
    }

    public void addCourse(Course course) {
        this.courses.add(course);
        course.addStudent(this);
    }

    @Override
    public String toString() {
        return "Student{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", lenght=" + length +
                ", birthday=" + birthday +
                '}' + (address == null ? "<no address>" : address)
                + (school == null ? "<no school>" : school);
    }
}
