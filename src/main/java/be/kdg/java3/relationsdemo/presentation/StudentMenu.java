package be.kdg.java3.relationsdemo.presentation;

import be.kdg.java3.relationsdemo.domain.Address;
import be.kdg.java3.relationsdemo.domain.Course;
import be.kdg.java3.relationsdemo.domain.School;
import be.kdg.java3.relationsdemo.domain.Student;
import be.kdg.java3.relationsdemo.repository.CourseRepository;
import be.kdg.java3.relationsdemo.repository.SchoolRepository;
import be.kdg.java3.relationsdemo.repository.StudentRepository;
import org.springframework.stereotype.Component;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Scanner;

@Component
public class StudentMenu {
    private Scanner scanner = new Scanner(System.in);
    private StudentRepository studentRepository;
    private SchoolRepository schoolRepository;
    private CourseRepository courseRepository;

    public StudentMenu(StudentRepository studentRepository, SchoolRepository schoolRepository, CourseRepository courseRepository) {
        this.studentRepository = studentRepository;
        this.schoolRepository = schoolRepository;
        this.courseRepository = courseRepository;
    }

    public void show() {
        while (true) {
            System.out.println("Welcome to the Student Management System");
            System.out.println("========================================");
            System.out.println("1) list all students");
            System.out.println("2) add a student");
            System.out.println("3) update a student");
            System.out.println("4) delete a student");
            System.out.println("5) change address of student");
            System.out.println("6) change school of student");
            System.out.println("7) list all students of school");
            System.out.println("8) delete a school");
            System.out.println("9) add student to course");
            System.out.println("10) list students of course");
            System.out.println("11) list courses of student");
            System.out.println("12) delete course");
            System.out.print("Your choice:");
            int choice = Integer.parseInt(scanner.nextLine());
            switch (choice) {
                case 1 -> listAllStudents();
                case 2 -> addStudent();
                case 3 -> updateStudent();
                case 4 -> deleteStudent();
                case 5 -> changeAddressOfStudent();
                case 6 -> changeSchoolOfStudent();
                case 7 -> showAllStudentsOfSchool();
                case 8 -> deleteSchool();
                case 9 -> addStudentToCourse();
                case 10 -> listStudentsOfCourse();
                case 11 -> listCoursesOfStudent();
                case 12 -> deleteCourse();
            }
        }
    }

    private void deleteCourse() {
        courseRepository.findAll().forEach(System.out::println);
        System.out.print("Course id:");
        int courseId = Integer.parseInt(scanner.nextLine());
        courseRepository.delete(courseId);
    }

    private void listCoursesOfStudent() {
        listAllStudents();
        System.out.print("Student id:");
        int studentid = Integer.parseInt(scanner.nextLine());
        Student student = studentRepository.findById(studentid);
        student.getCourses().forEach(System.out::println);
    }

    private void listStudentsOfCourse() {
        courseRepository.findAll().forEach(System.out::println);
        System.out.print("Course id:");
        int courseId = Integer.parseInt(scanner.nextLine());
        studentRepository.findByCourse(courseId).forEach(System.out::println);
    }

    private void addStudentToCourse() {
        listAllStudents();
        System.out.print("Student id:");
        int studentid = Integer.parseInt(scanner.nextLine());
        Student student = studentRepository.findById(studentid);
        courseRepository.findAll().forEach(System.out::println);
        System.out.print("Course id:");
        int courseId = Integer.parseInt(scanner.nextLine());
        Course course = courseRepository.findById(courseId);
        student.addCourse(course);
        studentRepository.update(student);
    }


    private void deleteSchool() {
        System.out.println("Id of school:");
        schoolRepository.findAll().forEach(System.out::println);
        int schoolid = Integer.parseInt(scanner.nextLine());
        try {
            schoolRepository.delete(schoolid);
        } catch (RuntimeException rte) {
            System.out.println("Unable to delete..." + rte.getMessage());
        }
    }

    private void showAllStudentsOfSchool() {
        System.out.println("Id of school:");
        schoolRepository.findAll().forEach(System.out::println);
        int schoolid = Integer.parseInt(scanner.nextLine());
        studentRepository.findBySchool(schoolid).forEach(System.out::println);
    }

    private void changeSchoolOfStudent() {
        listAllStudents();
        System.out.print("Student id:");
        int studentid = Integer.parseInt(scanner.nextLine());
        Student student = studentRepository.findById(studentid);
        if (student!=null) {
            changeSchoolOfStudent(student);
            studentRepository.update(student);
        }
    }

    private void changeAddressOfStudent() {
        listAllStudents();
        System.out.print("Studentid:");
        int studentId = scanner.nextInt();
        scanner.nextLine();//clear buffer...
        Student student = studentRepository.findById(studentId);
        if (student!=null) {
            System.out.print("Street:");
            String street = scanner.nextLine();
            System.out.print("Postal code:");
            int postalCode = scanner.nextInt();
            scanner.nextLine();//clear buffer...
            System.out.print("City:");
            String city = scanner.nextLine();
            Address address = new Address(street, postalCode, city);
            student.setAddress(address);
            studentRepository.update(student);
        }
    }

    private void listAllStudents() {
        try {
            studentRepository.findAll().forEach(System.out::println);
        } catch (RuntimeException dbe) {
            System.out.println("Unable to find all students:");
            System.out.println(dbe.getMessage());
        }
    }

    private void changeSchoolOfStudent(Student student){
        schoolRepository.findAll().forEach(System.out::println);
        System.out.println("(E)xisting or (N)ew school?");
        String choice = scanner.nextLine();
        School school = null;
        if (choice.equals("N")) {
            System.out.print("Name of school:");
            String schoolName = scanner.nextLine();
            school = schoolRepository.create(new School(schoolName));
        } else {
            System.out.println("Id of school:");
            int schoolid = Integer.parseInt(scanner.nextLine());
            school = schoolRepository.findById(schoolid);
        }
        student.setSchool(school);
    }

    private void addStudent() {
        System.out.print("Name:");
        String name = scanner.nextLine();
        System.out.print("Length:");
        double length = Double.parseDouble(scanner.nextLine());
        System.out.print("Birthday (dd-mm-yyyy):");
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd-MM-yyyy");
        LocalDate birthday = LocalDate.parse(scanner.nextLine(), formatter);
        Student student = new Student(name, length, birthday);
        changeSchoolOfStudent(student);
        try {
            Student createdStudent
                    = studentRepository.create(student);
            System.out.println("Student added to database:" + createdStudent);
        } catch (RuntimeException dbe) {
            System.out.println("Problem:" + dbe.getMessage());
        }
    }

    private void updateStudent() {
        listAllStudents();
        System.out.print("Id:");
        int id = Integer.parseInt(scanner.nextLine());
        System.out.print("Name:");
        String name = scanner.nextLine();
        System.out.print("Length:");
        double length = Double.parseDouble(scanner.nextLine());
        System.out.print("Birthday (dd-mm-yyyy):");
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd-MM-yyyy");
        LocalDate birthday = LocalDate.parse(scanner.nextLine(), formatter);
        Student student = new Student(id, name, length, birthday);
        changeSchoolOfStudent(student);
        try {
            studentRepository.update(student);
        } catch (RuntimeException dbe) {
            System.out.println("Problem:" + dbe.getMessage());
        }
    }

    private void deleteStudent() {
        listAllStudents();
        System.out.print("Id:");
        int id = Integer.parseInt(scanner.nextLine());
        try {
            studentRepository.delete(id);
        } catch (RuntimeException dbe) {
            System.out.println("Problem:" + dbe.getMessage());
        }
    }
}
