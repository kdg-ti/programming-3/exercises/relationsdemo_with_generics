package be.kdg.java3.relationsdemo.repository;

import be.kdg.java3.relationsdemo.domain.Course;

import java.util.List;

public interface CourseRepository extends EntityRepository<Course>{
}
