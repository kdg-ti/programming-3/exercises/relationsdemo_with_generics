package be.kdg.java3.relationsdemo.repository;

import be.kdg.java3.relationsdemo.domain.School;

import java.util.List;

public interface SchoolRepository extends EntityRepository<School>{
}
