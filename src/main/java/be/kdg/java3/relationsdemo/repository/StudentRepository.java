package be.kdg.java3.relationsdemo.repository;

import be.kdg.java3.relationsdemo.domain.Student;

import java.util.List;

public interface StudentRepository extends EntityRepository<Student> {
    public List<Student> findBySchool(int schoolId);
    List<Student> findByCourse(int courseId);
}
