package be.kdg.java3.relationsdemo.repository.jdbc;

import be.kdg.java3.relationsdemo.domain.Course;
import be.kdg.java3.relationsdemo.repository.CourseRepository;
import org.springframework.context.annotation.Profile;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.simple.SimpleJdbcInsert;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Repository
public class JDBCTemplateCourseRepository implements CourseRepository {
    private JdbcTemplate jdbcTemplate;
    private SimpleJdbcInsert courseInserter;

    public JDBCTemplateCourseRepository(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
        this.courseInserter = new SimpleJdbcInsert(jdbcTemplate)
                .withTableName("COURSES")
                .usingGeneratedKeyColumns("ID");
    }

    public Course mapCourse(ResultSet rs, int rowid) throws SQLException {
        return new Course(rs.getInt("ID"),
                rs.getString("NAME"), rs.getInt("ACADEMIC_YEAR"));
    }
    @Override
    public List<Course> findAll() {
        return jdbcTemplate.query("SELECT * FROM COURSES", this::mapCourse);
    }

    @Override
    public Course findById(int id) {
        return jdbcTemplate.queryForObject("SELECT * FROM COURSES WHERE ID=?",
                this::mapCourse, id);
    }

    @Override
    public Course create(Course course) {
        Map<String, Object> parameters = new HashMap<>();
        parameters.put("NAME", course.getName());
        parameters.put("ACADEMIC_YEAR", course.getAcademicYear());
        course.setId(courseInserter.executeAndReturnKey(parameters).intValue());
        return course;
    }

    @Override
    public void update(Course course) {
        jdbcTemplate.update("UPDATE COURSES SET NAME=?, ACADEMIC_YEAR=? WHERE ID=?",
                course.getName(), course.getAcademicYear(), course.getId());

    }

    @Override
    @Transactional
    public void delete(int id) {
        jdbcTemplate.update("DELETE FROM STUDENTS_COURSES WHERE COURSE_ID=?", id);
        jdbcTemplate.update("DELETE FROM COURSES WHERE ID=?", id);
    }
}
