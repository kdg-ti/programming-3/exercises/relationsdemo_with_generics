package be.kdg.java3.relationsdemo.repository.jdbc;

import be.kdg.java3.relationsdemo.domain.Course;
import be.kdg.java3.relationsdemo.domain.Entity;
import be.kdg.java3.relationsdemo.repository.EntityRepository;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.simple.SimpleJdbcInsert;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public abstract class JDBCTemplateEntityRepository<T extends Entity> implements EntityRepository<T> {
    private JdbcTemplate jdbcTemplate;
    private SimpleJdbcInsert entityInserter;

    public JDBCTemplateEntityRepository(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
        this.entityInserter = new SimpleJdbcInsert(jdbcTemplate)
                .withTableName(getTableName())
                .usingGeneratedKeyColumns("ID");
    }

    public abstract String getTableName();
    public abstract T mapEntity(ResultSet rs, int rowid) throws SQLException;
    public abstract Map<String, Object> getParameterMap(T entity);

    @Override
    public List<T> findAll() {
        return jdbcTemplate.query("SELECT * FROM " + getTableName(), this::mapEntity);
    }

    @Override
    public T findById(int id) {
        return jdbcTemplate.queryForObject("SELECT * FROM " + getTableName() + " WHERE ID=?",
                this::mapEntity, id);
    }

    @Override
    public T create(T entity) {
        Map<String, Object> parameters = getParameterMap(entity);
        entity.setId(entityInserter.executeAndReturnKey(parameters).intValue());
        return entity;
    }

    @Override
    public void update(T entity) {

    }

    @Override
    public void delete(int id) {

    }
}
