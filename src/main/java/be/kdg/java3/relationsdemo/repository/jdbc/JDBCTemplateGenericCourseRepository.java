package be.kdg.java3.relationsdemo.repository.jdbc;

import be.kdg.java3.relationsdemo.domain.Course;
import be.kdg.java3.relationsdemo.repository.CourseRepository;
import org.springframework.jdbc.core.JdbcTemplate;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

public class JDBCTemplateGenericCourseRepository
        extends JDBCTemplateEntityRepository<Course> implements CourseRepository {
    public JDBCTemplateGenericCourseRepository(JdbcTemplate jdbcTemplate) {
        super(jdbcTemplate);
    }

    @Override
    public String getTableName() {
        return "COURSES";
    }

    @Override
    public Course mapEntity(ResultSet rs, int rowid) throws SQLException {
        return new Course(rs.getInt("ID"),
                rs.getString("NAME"), rs.getInt("ACADEMIC_YEAR"));
    }

    @Override
    public Map<String, Object> getParameterMap(Course course) {
        Map<String, Object> parameters = new HashMap<>();
        parameters.put("NAME", course.getName());
        parameters.put("ACADEMIC_YEAR", course.getAcademicYear());
        return parameters;
    }
}
