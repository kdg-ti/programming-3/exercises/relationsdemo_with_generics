package be.kdg.java3.relationsdemo.repository.jdbc;

import be.kdg.java3.relationsdemo.domain.Address;
import be.kdg.java3.relationsdemo.domain.Course;
import be.kdg.java3.relationsdemo.domain.School;
import be.kdg.java3.relationsdemo.domain.Student;
import be.kdg.java3.relationsdemo.repository.StudentRepository;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.simple.SimpleJdbcInsert;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Repository
public class JDBCTemplateStudentRepository implements StudentRepository {
    private JdbcTemplate jdbcTemplate;
    private SimpleJdbcInsert studentInserter;

    public JDBCTemplateStudentRepository(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
        this.studentInserter = new SimpleJdbcInsert(jdbcTemplate)
                .withTableName("STUDENTS")
                .usingGeneratedKeyColumns("ID");
    }

    //Helper method: maps the columns of the DB to the attributes of the Student
    public Student mapStudentRow(ResultSet rs, int rowid) throws SQLException {
        return new Student(rs.getInt("ID"),
                rs.getString("NAME"),
                rs.getDouble("LENGTH"),
                rs.getDate("BIRTHDAY").toLocalDate());
    }

    public School mapSchoolRow(ResultSet rs, int rowid) throws SQLException {
        return new School(rs.getInt("ID"),
                rs.getString("NAME"));
    }

    public Course mapCourse(ResultSet rs, int rowid) throws SQLException {
        return new Course(rs.getInt("ID"),
                rs.getString("NAME"), rs.getInt("ACADEMIC_YEAR"));
    }

    public static Address mapAddressRow(ResultSet rs, int rowid) throws SQLException {
        return new Address(
                rs.getString("STREET"),
                rs.getInt("POSTAL_CODE"),
                rs.getString("CITY"));
    }

    @Override
    public List<Student> findAll() {
        List<Student> students = jdbcTemplate.query("SELECT * FROM STUDENTS",
                this::mapStudentRow);
        students.forEach(this::loadAddressAndSchool);
        students.forEach(this::loadCourses);
        return students;
    }

    @Override
    public Student findById(int id) {
        Student student = jdbcTemplate.queryForObject("SELECT * FROM STUDENTS WHERE ID = ?",
                this::mapStudentRow, id);
        if (student != null) {
           loadAddressAndSchool(student);
            loadCourses(student);
        }
        return student;
    }

    public List<Student> findBySchool(int schoolId){
        List<Student> students = jdbcTemplate.query("SELECT * FROM STUDENTS WHERE SCHOOL_ID=?",
                this::mapStudentRow, schoolId);
        students.forEach(this::loadAddressAndSchool);
        students.forEach(this::loadCourses);
        return students;
    }

    @Override
    public List<Student> findByCourse(int courseId) {
        List<Student> students = jdbcTemplate.query("SELECT * FROM STUDENTS WHERE ID IN " +
                "(SELECT STUDENT_ID FROM STUDENTS_COURSES WHERE COURSE_ID = ?)", this::mapStudentRow, courseId);
        //load the addresses:
        students.forEach(this::loadAddressAndSchool);
        students.forEach(this::loadCourses);
        return students;
    }

    private void loadCourses(Student student) {
        List<Course> courses = jdbcTemplate.query("SELECT * FROM COURSES WHERE ID IN " +
                "(SELECT COURSE_ID FROM STUDENTS_COURSES WHERE STUDENT_ID = ?) ", this::mapCourse, student.getId());
        student.setCourses(courses);
    }

    private void loadAddressAndSchool(Student student) {
        try {
            Address address = jdbcTemplate.queryForObject("SELECT * FROM ADDRESS WHERE STUDENT_ID=?",
                    JDBCTemplateStudentRepository::mapAddressRow, student.getId());
            student.setAddress(address);
        } catch (EmptyResultDataAccessException e) {
            //if the student has no address...
            //no problem, we do nothing!
        }
        try {
            School school = jdbcTemplate.queryForObject("SELECT SCHOOLS.ID, SCHOOLS.NAME " +
                    "FROM SCHOOLS INNER JOIN STUDENTS on SCHOOLS.ID = STUDENTS.SCHOOL_ID AND STUDENTS.ID = ?",
                    this::mapSchoolRow, student.getId());
            student.setSchool(school);
        } catch (EmptyResultDataAccessException e) {
            //if the student has no address...
            //no problem, we do nothing!
        }
    }

    @Override
    public Student create(Student student) {
        Map<String, Object> parameters = new HashMap<>();
        parameters.put("NAME", student.getName());
        parameters.put("LENGTH", student.getLength());
        parameters.put("BIRTHDAY", Date.valueOf(student.getBirthday()));
        parameters.put("SCHOOL_ID", student.getSchool().getId());
        student.setId(studentInserter.executeAndReturnKey(parameters).intValue());
        if (student.getAddress() != null) {
            Address address = student.getAddress();
            jdbcTemplate.update("INSERT INTO ADDRESS (STREET, POSTAL_CODE, CITY, STUDENT_ID) " +
                            "VALUES (?,?,?,?)", address.getStreet(), address.getPostalCode(),
                    address.getCity(),
                    student.getId());
        }
        return student;
    }

    @Override
    public void update(Student student) {
        jdbcTemplate.update("UPDATE STUDENTS SET NAME=?, LENGTH=?,BIRTHDAY=?, SCHOOL_ID=? WHERE ID=?",
                student.getName(), student.getLength(), Date.valueOf(student.getBirthday()),
                student.getSchool().getId(), student.getId());
        if (student.getAddress() != null) {
            Address address = student.getAddress();
            jdbcTemplate.update("MERGE INTO ADDRESS (STREET, POSTAL_CODE, CITY, STUDENT_ID) " +
                            "KEY (STUDENT_ID) VALUES (?, ?, ?, ?)",
                    address.getStreet(), address.getPostalCode(),
                    address.getCity(), student.getId());
        }
        jdbcTemplate.update("DELETE FROM STUDENTS_COURSES WHERE STUDENT_ID = ?",student.getId());
        for (Course course : student.getCourses()) {
            jdbcTemplate.update("INSERT INTO STUDENTS_COURSES VALUES ( ?,? )",student.getId(),course.getId());
        }
    }

    @Override
    @Transactional
    public void delete(int id) {
        jdbcTemplate.update("DELETE FROM ADDRESS WHERE STUDENT_ID=?", id);
        jdbcTemplate.update("DELETE FROM STUDENTS_COURSES WHERE STUDENT_ID=?", id);
        jdbcTemplate.update("DELETE FROM STUDENTS WHERE ID=?", id);
    }
}
